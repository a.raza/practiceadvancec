//
//  Box.h
//  AdvanceC
//
//  Created by Ammad Raza on 9/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//

#ifndef Box_h
#define Box_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Box
{
    int value;
    struct Box *next;

} Box, *Stack;

Stack * createStack(void);

void destroyStack(Stack *s);

void push (Stack *s, int val);

void pop(Stack *s);

int top(Stack *s);

int isEmpty(Stack *s);

#endif /* Box_h */
