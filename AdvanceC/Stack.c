//
//  Stack.c
//  AdvanceC
//
//  Created by Ammad Raza on 9/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//
//
/*
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Stack.h"

Stack *createStack(int size)
{
    Stack *res = (Stack *)malloc(sizeof(Stack));
    
    if(!res)
        return 0;
    
    //make new stack of the size
    res -> size = size;
    
    //start with zero
    res -> nElements = 0;
    
    //make the slots/index/array
    res -> element = (int *) malloc(size*sizeof(int));
    
    if(!res ->element)
    {
        free(res);
        return 0;
    }
    
    return res;
}

void destroyStack(Stack *s)
{
    //  destroy/clear elements in Stack
    free(s ->element);
    
    //  destory/clear the Stack
    free(s);
}

void push (Stack *s, int e)
{
//    printf("%d , %d",s, e);
    s-> nElements++;                //- can be done as following as in one line
    s-> element[s-> nElements-1]=e; //-     s-> elements[s->nElements++]=e;
}

void pop(Stack *s)
{
    s->element--;
}

int top(Stack *s)
{
    return s->element[s->nElements-1];
}

int isEmpty(Stack *s)
{
    return s->nElements==0;
}



int main()
{
    Stack *s = createStack(10);
    
    if(isEmpty(s))
        printf("Ok Empty Initially\n");
    
    int i;
    
    for(i = 0; i < 5; i++)
    {
        push(s, i);
//        printf(s, i);
    }
    
//        printf("%d \n", s, i);
    
    
    printf("%d\n", top (s));
    
    while (!isEmpty(s))
        pop(s);
    
    destroyStack(s);
    
    return 0;
}*/
