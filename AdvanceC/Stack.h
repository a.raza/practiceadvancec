//
//  Stack.h
//  AdvanceC
//
//  Created by Ammad Raza on 9/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//
/*
#ifndef Stack_h
#define Stack_h

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef  struct
{
    int size;
    int *element;
    int nElements;
 
} Stack;

Stack * createStack(int max);

void destroyStack(Stack *s);

void push (Stack *s, int val);

void pop(Stack *s);

int top(Stack *s);

int isEmpty(Stack *s);
*/

#endif /* Stack_h */
