//
//  Box.c
//  AdvanceC
//
//  Created by Ammad Raza on 9/28/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//

#include "Box.h"

Stack* createStack()
{
    Stack *res = (Stack*) malloc(sizeof(Stack));
    
    if(!res)
        return 0;
    
    *res = 0;
    
    return res;
}

void destroyStack(Stack *s)
{
    Stack temp;
    
    while (*s)
    {
        temp = (*s)->next; // Step 1
        free(*s);          // Step 2
        *s =temp;          // Step 3
    }
    
    free(s);
}

void push (Stack *s, int e)
{
    Stack temp = (Stack) malloc(sizeof(Box)); //allocate size of box    //Step 1
    
    temp -> value=e;    // |    Step
    temp -> next=*s;    // |      2

    *s=temp;
}

void pop(Stack *s)
{
    Stack temp = (*s) ->next;
    free(*s);
    *s = temp;
}

int top(Stack *s)
{
    return (*s) -> value;
}

int isEmpty(Stack *s)
{
//    return !(*s);
//        OR
    return *s == 0;
}

int main()
{
    Stack *s = createStack();
    
    if(isEmpty(s))
        printf("Ok Empty Initially\n");
    
    int i;
    
    for(i = 0; i < 5; i++)
    {
        push(s, i);
        //        printf(s, i);
    }
    
    //        printf("%d \n", s, i);
    
    
    printf("%d\n", top (s));
    
    while (!isEmpty(s))
        pop(s);
    
    destroyStack(s);
    
    return 0;
}

//For Next Time
//How to write generic code in C

void sortArray(int *a/*type*/, int size)
{
    int i, j;
    
    for(i = 0; i < size-1; i++)
    {
        for (j = i+1; j< size; j++)
        {
            if(a[i] >/*Compare*/ a[j])
            {
                int/*type of data*/ temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
}
