//
//  tArray.c
//  AdvanceC
//
//  Created by Ammad Raza on 9/21/18.
//  Copyright © 2018 Ammad Raza. All rights reserved.
//
/*
#include "tArray.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//strcpy, strlin
#include <stringlist.h>

int *createArray(int size)
{
    int * res = (int*) malloc(size *sizeof(int));
    
    if(!res)
        return 0;
    
    int i ;
    
    for(i= 0; i < size; i++)
    {
        res[i] = 0;
    }
    
    return res;
}

void displayArray(int*a , int size)
{
    printf("[");

    int i;
    for(i = 0; i < size; i++)
    {
        printf("%d", a[i]);
    }
    
    printf("]\n");
}

void sortArray(int *a, int size)
{
    int i, j;
    
    for(i = 0; i < size-1; i++)
    {
        for (j = i+1; j< size; j++)
        {
            if(a[i] > a[j])
            {
                int temp = a[i];
                a[i] = a[j];
                a[j] = temp;
            }
        }
    }
}

void destroyArray(int **a)
{
    free(*a);
    
    *a = 0;
}

///change test prog to look for int values in the command line
///(if no value, prog exits)


///adda "-s size" option to generate a random array of that size filled with values in O..
///int random();
///   |
///    --> O ... NAXINT
/// void srandom(int seed);       //s in srandom is seed


/// '==' will compare the base address of the array


int main(int nWords, const char *words[])
{
    int i;
    
    if(nWords == 1)
        return 1;
    
//    if(nWords == 0)
//    {
//        nWords = 10;
//        *words = 'Advance';
//    }
    
    int *a = createArray(nWords-1);
    
    if(!a)
        return 2;
    
    for (i = 1; i<nWords; i++)
    {
        sscanf(words[i], "%d", a + i - 1); // (a + i - 1) <=> & (a[i-1])
    }
    
//    int *a = createArray(10);
    
//    displayArray(a, 10);
    
//    a[0] = 1;
//    a[1] = 4;
//    a[2] = 9;
    
//    if(!string("-s", words[1]))
//    {
//        // generate random array
//    }
    
    displayArray(a, 10);
    
    sortArray(a, 10);
    
    displayArray(a, 10);
    
//    destroyArray(*a);
    
    printf("%p\n",a);
    
    return 0;
}
*/

//pass it by address


//Next Week:

//run the prog with increasing array size
//try to determine t=f(size)      t equals time
